<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function InicioProducto(Request $request)
    {
        //dd('hola mundo');
        $producto = Producto::all();
        
        return view('productos.inicio')->with('producto', $producto);
    }

    public function CrearProducto(Request $request){
        $producto = Producto::all();
        return view('productos.crear')->with('producto', $producto);
    }

    public function GuardarProducto(Request $request){

        $this->validate($request, [
            'nombre' => 'required',
            'tipo'   => 'required',
            'estado' => 'required',
            'precio' => 'required'
        ]);

        $producto = new Producto;
        $producto->nombre = $request->nombre;
        $producto->tipo   = $request->tipo;
        $producto->estado = $request->estado;
        $producto->precio = $request->precio;
        $producto->save();

        return redirect()->route('list.productos');
    }



    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
